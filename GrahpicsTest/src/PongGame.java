import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.*;

public class PongGame extends JPanel {

    //http://cs.lmu.edu/~ray/notes/javagraphics/

    //http://www.java-gaming.org/index.php?topic=24220.0

    // some values for esier editing
    private static final float player_move_speed = 1;
    private static final float pong_move_speed = 1;

    private static final int window_w = 450;
    private static final int window_h = 250;
    private static final int fps = 60;

    // what keys are used to controll movement
    private static final int Player1_upKey   = KeyEvent.VK_W;
    private static final int Player1_downKey = KeyEvent.VK_S;
    private static final int Player2_upKey   = KeyEvent.VK_UP;
    private static final int Player2_downKey = KeyEvent.VK_DOWN;

    // this is a handler for key input, every time any key is pressed the event will pass through here
    // this is where we will catch our controls for the game
    private KeyListener keyListener = new KeyListener() {
        // on ANY key down. when buttons are pressed and released we mark it and then in update loop
        // we check what is currently pressed and what should move where
        public void keyPressed(KeyEvent e) {
            int codeOfTheKeyThatWaPressed = e.getKeyCode();

            // check if it is the key we use and mark it as pressed
            switch (codeOfTheKeyThatWaPressed) {
                case Player1_upKey   : Player1_upKey_isDown   = true; break;
                case Player1_downKey : Player1_downKey_isDown = true; break;
                case Player2_upKey   : Player2_upKey_isDown   = true; break;
                case Player2_downKey : Player2_downKey_isDown = true; break;
            }
        }

        // on ANY key up
        public void keyReleased(KeyEvent e) {
            int codeOfTheKeyThatWaPressed = e.getKeyCode();

            // check if it is the key we use and mark it as released
            switch (codeOfTheKeyThatWaPressed) {
                case Player1_upKey   : Player1_upKey_isDown   = false; break;
                case Player1_downKey : Player1_downKey_isDown = false; break;
                case Player2_upKey   : Player2_upKey_isDown   = false; break;
                case Player2_downKey : Player2_downKey_isDown = false; break;
            }
        }

        // we dont use this but this has to be declared
        public void keyTyped(KeyEvent e) {}
    };

    // current state of keys (if down then do move)
    private static boolean Player1_upKey_isDown   = false;
    private static boolean Player1_downKey_isDown = false;
    private static boolean Player2_upKey_isDown   = false;
    private static boolean Player2_downKey_isDown = false;

    private static Color Base_Color       = Color.darkGray;
    private static Color Player1_Color    = Color.blue;
    private static Color Player2_Color    = Color.red;
    private static Color Pong_Color       = Base_Color;


    // window that will provide us place to draw
    private static JFrame window;

    public static void main(String[] args) {
        // create and initialize window
        window = new JFrame("Pong Game");
        // add game class to it
        window.getContentPane().add(new PongGame(), BorderLayout.CENTER);
        // when window closes exit application
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // self explanatory
        window.setSize(window_w, window_h);
        // show it right away
        window.setVisible(true);

        say("window created " + window.getWidth() + "x" + window.getHeight());
    }

    private static void say(String msg) {
        System.out.println(msg);
    }

    //-------------------

    // just a class to define a boxed shape and how to draw it
    class Box {
        // box properties
        float x, y, w, h;
        // have a color var so we could set individual color for box if we want to
        Color color = Base_Color;
        // constructor
        Box(float x, float y, float w, float h) {this.x = x;this.y = y;this.w = w;this.h = h; }
        // method that draws box in black to the drawing canvas
        void Draw() {
            drawingCanvas.setColor(color);
            drawingCanvas.drawRect((int)x, (int)y, (int)w, (int)h);
        }
        // just for easier referencing and more readable code
        float left()   { return x;     }
        float right()  { return x + w; }
        float top()    { return y;     }
        float bottom() { return y + h; }
        // move pong ()
        void Move(Movement m) { this.x += m.x; this.y += m.y; }
        // check if this box intersects with other box (b)
        boolean IntersectsWith(Box b) { return Math.max(0, Math.min(right(), b.right()) - Math.max(left(), b.left())) > 0
                && Math.max(0, Math.min(bottom(), b.bottom()) - Math.max(top(), b.top())) > 0; }
    }

    // class that holds information for movement with methods to simplify coding
    class Movement {
        float x, y;
        // c-tor
        Movement(float x, float y) { Set(x, y); }
        // to set var easily
        void Set(float x, float y) { this.x = x; this.y = y; }
        // to reverse movement Horizontal or Vertical
        void FlipX() { this.x *= -1; }
        void FlipY() { this.y *= -1; }
    }

    // class for our ball, extends Box so it is draw like box and has all properties and methods of box
    class Pong extends Box {
        public Pong() { super(100, 50, 10, 10); }
        // checks if pong is colliding with top or bottom edges of field, used for witching direction of movement
        public boolean IsAtTheTopOrBottomEdge() { return pong.top() <= field.top() || pong.bottom() >= field.bottom(); }
        // these two check if ball collides with left or right sides of field, used for scoring
        public boolean IsAtTheLeftEdge() { return pong.left() <= field.left(); }
        public boolean IsAtTheRightEdge() { return pong.right() >= field.right(); }
        // checks if pong colides with player, used for deflection
        public boolean IntersectsWithPlayer(Player p) { return IntersectsWith(p); }
    }

    // class for our Player, extends Box so it is draw like box and has all properties and methods of box
    class Player extends Box {
        // player id, duh
        public int id = 0;
        // >_>
        public int score = 0;
        // c-tor
        public Player(int id) {
            // if first player then put box left side, otherwise right side
            super(id == 1 ? field.left() : field.right() - 5, 50, 5, 20);
            this.id = id;
        }
        // this draws little cubes on the side of the player to indicate his score
        public void DrawScore() {
            for (int i = 0; i < score; i++) {
                drawingCanvas.setColor(color);
                if (id == 1)
                    drawingCanvas.drawRect(10 + 5 * i, 10, 3, 3);
                else
                    drawingCanvas.drawRect((int) field.w - 10 - 5 * i, (int) field.h - 10, 3, 3);
            }
        }
        public void MoveUp()   { this.y = Math.max(this.y - player_move_speed, field.top()); }
        public void MoveDown() { this.y = Math.min(this.y + player_move_speed, field.bottom() - this.h); }
        public void UpdateMovement() {
            if (id == 1 && Player1_upKey_isDown || id == 2 && Player2_upKey_isDown)
                MoveUp();
            if (id == 1 && Player1_downKey_isDown || id == 2 && Player2_downKey_isDown)
                MoveDown();
        }
    }

    //-------------------

    // this is timer to call our main loop in set intervals
    private Timer timer;

    // this is little class that will be used by timer, timer will call function 'run' from it
    private class GameLoop extends TimerTask {
        public void run() {
            // we simply call main loop, this whole thing is like a wrapper, seems like you have to to it this way in java if you want to use timers
            MainLoop();
        }
    }

    // this is 2d space for drawing, it has simple srawing stuff like rects ovals lines etc
    private Graphics2D drawingCanvas;

    // our field where game happens
    private Box field;
    // ball for our game
    private Pong pong;
    // our players on both sides
    private Player player1;
    private Player player2;

    // movement information for our pong
    private Movement movement;

    // --- GAME LOGIC BELOW ---

    private PongGame() {
        /*
        -1- init vars
        -2- colors
        -3- game loop timer
        -4- input for keys
         */

        // -1- initiate game elements
        field = new Box(10, 10, 400, 200);
        pong = new Pong();
        player1 = new Player(1);
        player2 = new Player(2);
        movement = new Movement(1f, 1f);

        // -2- make stuff pwetty
        pong.color = Pong_Color;
        player1.color = Player1_Color;
        player2.color = Player2_Color;

        // -3- create timer
        timer = new Timer();
        // tell timer what to call and how often, 1 - our task game loop 2 - with no delay (0) 3 - every X milliseconds (1s=1000ms / by frames per second)
        timer.schedule(new GameLoop(), 0, 1000 / fps);

        // -4- add a key listener we defined earlier to window to catch key press and release
        window.addKeyListener(keyListener); // end of window.addKeyListener
    }

    // this is games main loop function, it calls other update functions
    private void MainLoop() {
        // this check if our place for drawing is not set yet
        if (drawingCanvas == null) {
            // this tries to set it, if it fails it will be null again and will try next window
            drawingCanvas = (Graphics2D)getGraphics();
        }

        // if we have a place to draw then :
        if (drawingCanvas != null) {
            // clear previously drawn stuff
            drawingCanvas.clearRect(0, 0, window_w, window_h);
            // update game logic
            GameUpdate();
            // draw game objects
            GameRender();
        }
    }

    // -----------------

    void GameUpdate() {
        // update movement
        pong.Move(movement);
        player1.UpdateMovement();
        player2.UpdateMovement();

        // check and resolve collisions

        if (pong.IsAtTheTopOrBottomEdge()) {
            movement.FlipY();
        }

        if (pong.IsAtTheLeftEdge()) {
            movement.FlipX();
            player2.score++;
        }

        if (pong.IsAtTheRightEdge()) {
            movement.FlipX();
            player1.score++;
        }

        if (pong.IntersectsWithPlayer(player1) || pong.IntersectsWithPlayer(player2)) {
            movement.FlipX();
        }
    }
    void GameRender() {
        // draw game elements
        field.Draw();
        pong.Draw();
        player1.Draw();
        player2.Draw();

        // draw score dots
        player1.DrawScore();
        player2.DrawScore();
    }

}
